const express = require('express');
const app = express();
app.listen(3000, () =>{
   console.log('listening');
});

app.use(express.static('public'));

app.get('/main', (request, response) =>{
   response.sendFile(__dirname + '/index.html');
});
app.get('/search/:car', (request, response) => {
   let data = request.params;
   response.send('The brand of the ' + data.car + ' car is cool');
});

